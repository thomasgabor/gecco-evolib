import sys
sys.path += ['../']
from evolib import *
from explib import *
from divlib import *
from problib import *

problem_size = 4
diversity_weight = 20
exp = Experiment(RastriginProblem(problem_size), pop_size=30, num_gens=1000, num_runs=50, save_file=True)
exp.add_test().with_style(ls='-')
exp.perform(individual_class=Individual, style=dict(color='k'), label='non-diverse')
exp.perform(individual_class=FitnessSharingIndividual, alpha=2.0, style=dict(color='y'), label='fitness sharing')
exp.perform(individual_class=InheritedFitnessIndividual, inheritance_weight=0.2, style=dict(color='m'), label='inherited fitness')
exp.perform(individual_class=ManhattanDiversityIndividual, diversity_weight=diversity_weight, style=dict(color='b'), label='manhattan diverse')
exp.perform(individual_class=GenealogicalDiversityIndividual, tbits=16, diversity_weight=diversity_weight, style=dict(color='r'), label='genealogical diverse')
exp.perform(individual_class=TrueGenealogicalDistanceIndividual, diversity_weight=diversity_weight, style=dict(color='orange'), label='exact genealogical diverse')
exp.perform(population_class=EnsemblePopulation, individual_class=Individual, subpops=3, style=dict(color='g'), label='ensemble')
exp.print_verdict()
exp.plot()
exp.print_examples()