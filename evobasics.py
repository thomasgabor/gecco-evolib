import copy
import numpy as np

def randf(**kwargs):
    dim = ('dim' in kwargs) and kwargs['dim'] or 1
    min_val = ('min' in kwargs) and kwargs['min'] or 0
    max_val = ('max' in kwargs) and kwargs['max'] or 1
    choice = np.random.random_sample()
    return (max_val - min_val) * choice + min_val

def randi(**kwargs):
    dim = ('dim' in kwargs) and kwargs['dim'] or 1
    min_val = ('min' in kwargs) and kwargs['min'] or 0
    max_val = ('max' in kwargs) and kwargs['max'] or 1
    choice = np.random.randint(min_val, max_val)
    return choice
    
def randb(chance=0.5):
    return randf() < chance

def rando(option1, option2, chance=0.5):
    if randb(chance):
        return option1
    else:
        return option2

def randelem(xs):
    return xs[randi(max=len(xs))]
    
def randsel(xs, amount=1):
    return np.random.choice(xs, min(len(xs), amount), replace=False)

def cp(object):
    return copy.deepcopy(object)
    
def compact(some_list):
    new_list = []
    for item in some_list:
        if item:
            new_list.append(item)
    return new_list

def ifthenelse(condition, consequence, antisequence):
    if condition:
        return consequence
    else:
        return antisequence

def avg(numbers):
    return float(sum(numbers)) / float(max(len(numbers), 1))