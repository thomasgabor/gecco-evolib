import sys
import os
import time
import dill
import __main__
import explib


experiment_dill = sys.argv[1]
exp = dill.load(open(sys.argv[1], 'rb'))
explib.Experiment.plot(exp, save_file=False, plot_std=True, plot_log=True)