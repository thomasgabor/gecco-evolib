from evobasics import *

class Individual:
    next_id = 0
    def __init__(self, problem_instance, genome=None, **kwargs):
        self.args = kwargs
        self.problem_instance = problem_instance
        self.genome = genome or problem_instance.generate()
        self.initialize_more(**self.args)
        self.fitness = None
        self.id = Individual.next_id
        Individual.next_id += 1
    
    def problem(self):
        return self.problem_instance
    
    def initialize_more(self, **kwargs):
        pass
    
    def mutate(self):
        return type(self)(self.problem_instance, self.problem_instance.mutate(self.genome), **self.args)
    
    def recombine(self, other):
        return type(self)(self.problem_instance, self.problem_instance.recombine(self.genome, other.genome), **self.args)
    
    def evaluate(self, population):
        self.fitness = self.compute_fitness(population)
        return self.fitness

    def get_fitness(self):
        return self.fitness

    def compute_fitness(self, population):
        return self.compute_problem_fitness()

    def compute_problem_fitness(self):
        return self.problem_instance.evaluate(self.genome)
    
    def to_str(self):
        result = ''
        result += '<' + str(self.__class__.__name__) 
        result += ' ' + str(self.genome)
        result += str(self.to_str_more() or '')
        result += ' @ ' + str(self.compute_problem_fitness()) 
        result += ' (' + str(self.get_fitness())
        result += ')>'
        return result
        
    def to_str_more(self):
        return ''
    
    def __str__(self):
        return self.to_str()


class Population:
    def __init__(self, problem_instance, individuals=None, **kwargs):
        self.args = kwargs
        self.problem_instance = problem_instance
        if type(individuals) == list:
            self.individual_class = kwargs.get('individual_class', (len(individuals) > 0) and type(individuals[0]) or Individual)
            self.individuals = individuals
        elif type(individuals) == int:
            self.individual_class = kwargs.get('individual_class', Individual)
            self.individuals = self.generate_all(individuals)
        else:
            self.individuals = []
        self.size = len(self.individuals)
        self.mig_rate = self.args.get('mig', 0.1)
        self.mut_rate = self.args.get('mut', 0.1)
        self.rec_rate = self.args.get('rec', 0.3)
        self.par_rate = self.args.get('par', 0.3)

    def generate(self):
        return self.individual_class(self.problem_instance, **self.args)
        
    def generate_all(self, size):
        return [self.generate() for _ in range(size)]

    def best(self):
        return self.individuals[0]

    def evaluate(self):
        current_population = cp(self)
        self.individuals.sort(reverse=self.problem_instance.maximizing(), key=lambda individual: individual.evaluate(current_population))
        return self

    def select(self, size=None):
        size = size or self.size
        self.evaluate()
        self.individuals = self.individuals[0:size]
        return self
    
    def choose_mate(self, individual):
        return self.individuals[randi(max=int(self.par_rate * self.size))]
        
    def choose_random(self):
        return self.individuals[randi(max=self.size)]

    def invoke_mutation(self, individual):
        return individual.mutate()

    def mutate(self):
        self.individuals = [randb(self.mut_rate) and self.invoke_mutation(individual) or cp(individual) for individual in self.individuals]
        return self
    
    def invoke_recombination(self, individual):
        return individual.recombine(self.choose_mate(individual))
    
    def recombine(self):
        self.evaluate()
        self.individuals += compact([randb(self.rec_rate) and self.invoke_recombination(individual) for individual in self.individuals])
        return self
        
    def augment(self):
        self.individuals += [self.generate() for _ in range(int(self.mig_rate * self.size))]
        return self
        
    def evolve(self):
        self.recombine()        
        self.mutate()        
        self.augment()        
        self.select()
      